(defpackage #:pegbit
  (:use #:cl)
  (:export
   #:*stream*
   #:*rules*
   #:*eof*
   #:*parse-debug*
   #:*named-places-number*

   #:compile-parser
   ))
(in-package #:pegbit)


(defparameter *allow-error* nil)
(defparameter *stream* nil)
(defparameter *rules* nil)
(defvar *eof* :eof)


(defun merror (message)
  (if *allow-error*
      'nil
      (error "~a~%remaining:~%~a~%~%read:~%"
             message
             (input-stream-string *stream*))))

(defun enlist (x)
  (if (listp x) x (list x)))

(defun silence (&rest rest)
  (declare (ignore rest)))

(defun everyt (item tree)
  (if (atom tree)
      (eql item tree)
      (every (lambda (x) (everyt item x)) tree)))

(defun combine-words (s)
  (format nil "~{~a~^ ~}" s))

(defun char->symbol (c)
  (intern (format nil "~@(~c~)" c) :keyword))


(defun group->symbol (s)
  (char->symbol (char s 0)))

(defun input-stream-string (stream)
  (with-output-to-string (s)
    (loop :for l := (read-line stream nil)
          :while l
          :do (format s "~a~%" l))))

(defun parse-char (c)
  (list
   (let ((cc (read-char *stream*)))
     (if (char= cc c)
         (make-string 1 :initial-element c)
         (merror "Couldn't parse char")))))

(defun parse-eof ()
  (if (eq :eof (peek-char nil *stream* nil :eof))
      (list nil)
      (merror "Couldn't read EOF")))

(defun parse-char-not (char-bag)
  (list
   (let ((c (read-char *stream*)))
     (if (member c char-bag)
         (merror "Didn't read the correct char")
         (make-string 1 :initial-element c)))))

(defun parse-chars (char-bag)
  (list
   (let ((c (read-char *stream*)))
     (if (position c char-bag)
         (make-string 1 :initial-element c)
         (merror "Didn't read the correct char")))))

(defun parse-string (s)
  (list
   (loop :for i :from 0 :below (length s)
         :do (or (parse-char (char s i)) (merror "Couldn't parse string"))
         :finally (return s))))

(defun string-list-p (obj)
  (and (listp obj)
       (every #'stringp obj)))

(defun flatten-string-list (list)
  (apply #'concatenate 'string list))

(defun try-flatten-list (list)
  (if (string-list-p list)
      (list (flatten-string-list list))
      list))

(defun parse-symbol (s)
  (let ((r (funcall (or (cdr (assoc s *rules*)) (merror "Couldn't parse symbol")))))
    (list
     (if (string-list-p r)
         (list s (flatten-string-list r))
         (cons s r)))))

(defmacro try-parse (&body body)
  (let ((!save (gensym "SAVE")))
    `(let ((,!save (file-position *stream*)))
       (handler-case (progn ,@body)
         (error ()
           (file-position *stream* ,!save)
           nil)))))

(defvar *parse-debug* nil)
(defvar *named-places-number* 10)

(defun expand-tree (tree expansion-alist)
  (if (atom tree)
      tree
      (let ((r (assoc (car tree) expansion-alist)))
        (if r
            (apply (cdr r)
                   (mapcar (lambda (x) (expand-tree x expansion-alist))
                           (cdr tree)))
            (cons (expand-tree (car tree) expansion-alist)
                  (expand-tree (cdr tree) expansion-alist))))))

(defmacro transformers (&rest specs)
  "Uses the specs to walk body and transform according to each SPEC. If one of
the forms isn't recognized it is left alone. This is NOT a code walker. It is,
at best, a tree walker."
  (labels ((spec->transformer (spec)
             (cond ((atom spec)
                    ;; leave it alone
                    `(cons ',spec
                           (lambda (&rest rest)
                             (cons ',spec
                                   (remove "" (remove nil rest)
                                           :test (lambda (x y)
                                                   (and (stringp x)
                                                        (stringp y)
                                                        (string= x y))))))))
                   ((null (cdr spec))
                    `(cons ',(car spec)
                           (lambda (&rest rest)
                             (declare (ignore rest)))))
                   (t
                    ;; this one is a bit involved, we BOTH (1) bind the cadr, if
                    ;; it exists to entire match, and bind parts of the matches
                    ;; to *1*, *2*, ... *10*
                    (let ((!symbol-list (gensym))
                          (!x (if (cadr spec) (caadr spec) (gensym)))
                          (!xx (gensym))
                          (!rest (gensym))
                          (symbol-list (loop :for i :from 1 :to *named-places-number*
                                             :collect (intern (format nil "*~A*" i)))))
                      `(cons ',(car spec)
                             (lambda (&rest ,!rest)
                               (setf ,!rest
                                     (remove "" (remove nil ,!rest)
                                             :test (lambda (x y)
                                                     (and (stringp x)
                                                          (stringp y)
                                                          (string= x y)))))
                               (if *parse-debug*
                                   ,!rest
                                   (let* ((,!x ,!rest)
                                          (,!symbol-list ',symbol-list)
                                          ;; make a list of NAME-PLACES-NUMBER
                                          ;; long for progv, ie, any of the
                                          ;; values will be bound NIL
                                          (,!xx (subseq (append ,!x (make-list *named-places-number*)) 0 *named-places-number*)))
                                     (declare (special ,@symbol-list))
                                     (progv ,!symbol-list ,!xx
                                       (let ((,!xx (list ,@symbol-list)))
                                         (declare (ignorable ,!xx))
                                         ;; The rest of your code here, where *1*, *2*, etc., are bound to the values from input-list.
                                         ;; This is the scope where the bindings are valid.
                                         ,@(cddr spec))))))))))))
    `(list ,@(mapcar #'spec->transformer specs))))

(defmacro with-processors ((&rest specs) &body body)
  (let* ((flets
           (loop :for f :in specs
                 :collect
                 (cond ((atom f)
                        ;; leave it alone
                        `(,f (&rest rest)
                             (cons ',f (remove "" (remove nil rest)
                                               :test (lambda (x y)
                                                       (and (stringp x)
                                                            (stringp y)
                                                            (string= x y)))))))
                       ((null (cdr f))
                        `(,(car f) (&rest rest)
                          (declare (ignore rest))))
                       (t (let ((!symbol-list (gensym))
                                (!x (if (cadr f) (caadr f) (gensym)))
                                (!xx (gensym))
                                (symbol-list (loop :for i :from 1 :to *named-places-number*
                                                   :collect (intern (format nil "*~A*" i)))))
                            `(,(car f) (&rest rest)
                              (funcall
                               (if *parse-debug*
                                   #'identity
                                   (lambda (,!x)
                                     (let ((,!symbol-list ',symbol-list)
                                           ;; make a list of NAME-PLACES-NUMBER
                                           ;; long for progv, ie, any of the
                                           ;; values will be bound NIL
                                           (,!xx (subseq (append ,!x (make-list *named-places-number*)) 0 *named-places-number*)))
                                       (progv ,!symbol-list ,!xx
                                         (let ((,!xx (cons 'list ,!symbol-list)))
                                           (declare
                                            (special ,@symbol-list ,!xx))
                                           ;; The rest of your code here, where *1*, *2*, etc., are bound to the values from input-list.
                                           ;; This is the scope where the bindings are valid.
                                           ,@(cddr f))))))
                               (remove "" (remove nil rest)
                                       :test (lambda (x y)
                                               (and (stringp x)
                                                    (stringp y)
                                                    (string= x y))))))))))))
    `(flet ,flets
       ,@body)))

(defmacro compile-rule (spec)
  (labels ((s->l (s)
             (rec s))
           (s->tl (s)
             `(try-parse ,(rec s)))
           (rec (s)
             (etypecase s
               (null nil)
               ((eql :dot) `(parse-char-not '()))
               ((eql :eof) `(parse-eof))
               (character `(parse-char ,s))
               (string `(parse-string ,s))
               (symbol `(parse-symbol ',s))
               (cons (ecase (intern (symbol-name (car s)) '#:pegbit)
                       (? `(try-parse
                             ,(rec `(and ,@(cdr s)))))
                       (* (let ((!f (gensym "F"))
                                (!r (gensym "R")))
                            `(flet ((,!f ()
                                      ,(rec `(and ,@(cdr s)))))
                               (let ((,!r (loop :for ,!r := (try-parse (,!f))
                                                :while ,!r
                                                :append ,!r)))
                                 (try-flatten-list ,!r)))))
                       (+ (let ((!f (gensym "F"))
                                (!r (gensym "R")))
                            `(flet ((,!f ()
                                      ,(rec `(and ,@(cdr s)))))
                               (let ((,!r (append
                                           (,!f)
                                           (loop :for ,!r := (try-parse (,!f))
                                                 :while ,!r
                                                 :append ,!r))))
                                 (try-flatten-list ,!r)))))
                       (^ (if (every #'characterp (cdr s))
                              `(parse-char-not ',(cdr s))
                              (merror "^ can only be used with chars")))
                       (and `(append ,@(mapcar #'s->l (cdr s))))
                       (or `(or ,@(mapcar #'s->tl
                                          (butlast (cdr s)))
                                ,(s->l (car (last (cdr s))))))
                       ;; expects a string
                       ([] `(parse-chars ,(cadr s))))))))
    `(cons ',(car (enlist (car spec))) (lambda () ,(rec (cons 'and (cdr spec)))))))

(defmacro compile-parser (entry specs)
  "ENTRY is the first rule the parse will try/enter.

The specs are of the form:

(RULE-NAME RULE)

RULE-NAME can be

SYMBOL: which will be the head of the list with what is matched
(SYMBOL): means that what ever is matched isn't captured, it's discarded
(SYMBOL):

"
  (let ((!s (gensym "S"))
        (rules (loop :for s :in specs
                     :collect `(compile-rule ,s))))
    `(lambda (,!s)
       (let ((*rules* (list ,@rules)))
         (expand-tree (with-input-from-string (*stream* ,!s)
                        (car (parse-symbol ',entry)))
                      (transformers ,@ (mapcar #'first specs)))))))

