(defpackage #:pegbit-test
  (:use #:cl #:pegbit))

(in-package #:pegbit-test)

(defparameter *test-recipe*
  "Kung Pao Chicken
================
	-  bowl ------------- 2
Sauce
	a) soy -------------- (1/2 cup) 1 part
	a) sugar ------------ (1 cup) 2 parts
	a) water ------------ (2 cups) 4 parts
	a) chicken bouillon - (1 Tbsp.) 1/8 part
	a) corn starch ------ (2 Tbsp.) 1/4 part
Chicken
        -  oil -------------- enough to fry
	b) chicken (cubed) -- 2 lbs.
	b) corn starch ------ 1/4 cup
	b) flour ------------ 1/4 cup
	b) garlic powder ---- 1 tsp
	b) ginger powder ---- 1/2 tsp
	b) paprika ---------- 1/2 tsp
-----------------------------------------------
- Begin heating up the oil in a pan (to shallow fry) or a pot (to deep fry)
- Mix together 'a' in a sauce pan, bring to a boil, lower to a simmer, cook
  until thickened
- Mix together 'b'")

(defun combine-words (s)
  (format nil "~{~a~^ ~}" s))

(defparameter *recipe-parser*
  (compile-parser
   recipe
   ((recipe name nl rdelim nl sections sdelim nl steps)
    (sections (+ section))
    (section (or ingredients (and name nl ingredients)))
    (ingredients (+ ingredient nl))
    (ingredient ws group ws ingredient-name ws sdelim ws quantity)
    ((group (x) (list 'group (group->symbol (first x))))
     (or (and :dot #\))
         #\-))
    (ingredient-name words)
    (steps (* :step))
    (:step
     dash ws (+ sword) (? nl) (* ~dash (* ws) (+ sword) (? nl)))
    (-sline dash ws line)
    (sline (* ws) (* ~dash line (? nl)))
    ((words (x) (combine-words x))
     word (* ws word))
    ((word () *1*) (^ #\-) (+ (^ #\space #\newline #\tab)))
    ;; step escape word
    ((seword (x) (pegbit::char->symbol (char (car x) 0)))
     apostrophe (^ #\') apostrophe)
    ;; step word
    ((sword () *1*) (* ws) (or seword (+ (^ #\newline #\space #\tab))) (* ws))
    (quantity line)
    (name line)
    ((line () *1*) (+ (^ #\newline)))
    ((ws) (+ (or tab sp)))
    ((tab) #\tab)
    ((sp) #\sp)
    ((nl) #\newline)
    ((apostrophe) #\')
    ((rdelim) (+ #\=))
    ((~dash (x) (if (member (char (car x) 0) '(#\space #\newline #\tab))
                    nil
                    (car x)))
     (^ #\-))
    ((dash) #\-)
    ((sdelim) (+ #\-)))))

(defun group->symbol (s)
  (pegbit::char->symbol (char s 0)))

(defun parse-recipe (&optional recipe)
  (setf recipe (or recipe  *test-recipe*))
  (eval `(pegbit::with-processors
             ((recipe (x) (apply #'append x))
              (name (x) (list :name (car x)))
              (sections (x) (list :sections x))
              (section (x) (apply #'append x))
              (ingredients (x) (list :ingredients x))
              (ingredient (x) (apply #'append x))
              (group (x) (cons :group x))
              (ingredient-name (x) (cons :name x))
              (quantity (x) (cons :quantity x))
              (steps (x) (list :steps x))
              (:step (x) x))
           ,(funcall *recipe-parser* recipe))))

(defparameter *spell-cost-parser*
  (compile-parser
   spell-cost
   (;; the atoms
    ((color () (intern *1*))
     ([] "WRMBCGYLX"))
    ((variable () (intern *1*)) (or #\X #\Y #\Z))
    (digit ([] "0123456789"))
    ((number (x) (parse-integer (format nil "~{~a~}" (mapcar #'second x))))
     (+ digit))
    ((spaces) (or :eof (+ #\space)))
    ((:spaces) (+ #\space))
    ((value () *1*) (or number variable))
    ((scale () *1*) number)

    ;; the groupings
    ((card () *2*) #\[ color #\])
    ((number-range () (list 'number-range *1* (or *3* *1*)))
     (or (and number #\- number) number))
    ((variable-cost ()
                    (cons 'variable-cost
                          (if *2*
                              (list *2* *1*)
                              (list *1* 1))))
     (or (and scale variable)
         variable))
    ((value-cost () (list 'value-cost (or *1* 1) 1))
     (? value))
    ((color-cost () (append (list 'color-cost *2*) (list *1*)))
     (or variable-cost value-cost) color)
    ((card-cost () (list 'card-cost *2* *1*))
     (or variable-cost value-cost) card)
    ((cost () *1*) (or card-cost color-cost))
    ((required-costs () (list 'required-costs *2*))
     #\( (+ cost (? :spaces)) #\))
    ((let-spec () (list 'let-spec *1* *3*))
     variable #\: number-range)
    (spell-cost (* (or let-spec required-costs cost) spaces) :eof)))
  "Parses \"Spell Costs\" from a toy project/game some examples of costs:
3C          = 3 Cyan Mana
3[C]        = 3 Cyan Cards
X:2-5 XB YG = 2 to 5 Blue Mana, and Y amount of Green mana
")


(defparameter *basic-lisp-parser*
  (compile-parser
   form
   (((form () *1*) (or atom cons))
    ((atom () *1*) symbol)
    ((symbol (x) (list 'symbol (reduce (lambda (x y) (concatenate 'string x y)) x)))
     (+ (or escaped-word escaped-letter letter)))
    ((letter () (string-upcase *1*)) (^ #\# #\\ #\" #\, #\` #\| #\( #\) #\space #\tab #\newline #\linefeed #\return))
    (escaped-letter (and "\\" :dot))
    (vertical-bar #\|)
    ((escaped-word () *2*)
     (and vertical-bar (* (or escaped-letter (^ #\|))) vertical-bar))

    ((cons () (list* 'list* *2* *3*)) (and "(" (* ws) car (* ws) cdr))
    ((car () *1*) form)
    ((cdr () *1*)
     (or cdr-end
         cdr-dot
         cdr-tail))
    ((cdr-end () (list nil)) (and (* ws) ")"))
    ((cdr-dot () *1*) (and (* ws) dot (* ws) cdr))
    ((cdr-tail () (cons *1* *2*)) (and (* ws) form (* ws) cdr))
    ((dot) #\.)
    ((ws) (or #\space #\tab #\return #\newline #\linefeed)))))


(defun parse-basic-lisp (&optional lisp-form)
  (setf lisp-form (or lisp-form "(a . (b c . d))"))
  (funcall *basic-lisp-parser* lisp-form))
