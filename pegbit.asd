(asdf:defsystem "pegbit"
  :description "pegbit: A simple library for creating a PEG parser from simple rule specs."
  :version "0.0.1"
  :author "Juan-Andres Martinez <rgbit@protonmail.com>"
  ;; :depends-on ()
  :serial t
  :components ((:file "pegbit")))
